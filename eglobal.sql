-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.25a - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-11-17 20:24:04
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table eglobal.advert
CREATE TABLE IF NOT EXISTS `advert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `add_datetime` datetime NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_54F1F40B7E3C61F9` (`owner_id`),
  CONSTRAINT `FK_54F1F40B7E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table eglobal.advert: ~0 rows (approximately)
/*!40000 ALTER TABLE `advert` DISABLE KEYS */;
INSERT INTO `advert` (`id`, `title`, `text`, `add_datetime`, `owner_id`) VALUES
	(1, 'Buy cat !', 'I will buy cat in the bag !', '2013-11-17 18:56:40', 7),
	(2, 'Buy car !', 'Need car for 100 usd', '2013-11-15 19:56:28', 7),
	(3, 'Sell boot !', 'One red, second white !  only for 20 USD', '2013-11-16 15:47:46', 7),
	(4, 'Trade', 'Trade something for something !', '2013-11-16 16:56:40', 4),
	(5, 'Buy PC', 'Want to buy 256 Mhz windows 3.1 - 1000 USD', '2013-11-16 17:55:35', 4);
/*!40000 ALTER TABLE `advert` ENABLE KEYS */;


-- Dumping structure for table eglobal.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `isActive` tinyint(1) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table eglobal.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `salt`, `roles`, `isActive`, `email`) VALUES
	(4, 'max', 'ehstTdzLp8w58t2+I1P+0DNCzyPX+cg1gZGRsYrcsUBL9SKWXkcUiYRxy81M9gX6o/Th+KchTJYf2vWfd73ebQ==', 'jle3i45m2s088w00s8cokcsgsogw8k8', 'a:1:{i:0;s:9:"ROLE_USER";}', 1, 'max@max.lv'),
	(7, 'max1', 'MoMQI0B0YJZZcrkfrGKE7P7qhdr3i/rZztBituSQwes/ToyzqITnaqJ0Bb6rifzhkU0YqREZ8qmjcbtC/dZIQA==', '7lol1meemjk0osw0ok0oks44sow0oo0', 'a:1:{i:0;s:9:"ROLE_USER";}', 1, 'max1@max.lv');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
