<?php

namespace Eglobal\AdvertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Eglobal\UserBundle\Entity\User;

/**
 * Advert
 *
 * @ORM\Table(name="advert")
 * @ORM\Entity(repositoryClass="Eglobal\AdvertBundle\Entity\AdvertRepository")
 */
class Advert
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="title")
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text", name="text")
     */
    private $text;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="add_datetime")
     */
    private $addDatetime;

    /**
     * @ORM\ManyToOne(targetEntity="Eglobal\UserBundle\Entity\User")
     *
     */
    protected $owner;

    /**
     * @param mixed $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Advert
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Advert
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set addDatetime
     *
     * @param \DateTime $addDatetime
     * @return Advert
     */
    public function setAddDatetime($addDatetime)
    {
        $this->addDatetime = $addDatetime;
    
        return $this;
    }

    /**
     * Get addDatetime
     *
     * @return \DateTime 
     */
    public function getAddDatetime()
    {
        return $this->addDatetime;
    }




}
