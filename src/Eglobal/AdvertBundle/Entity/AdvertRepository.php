<?php

namespace Eglobal\AdvertBundle\Entity;

use Doctrine\ORM\EntityRepository;


class AdvertRepository extends EntityRepository{
    public function findAllAdverts()
    {
        return $this->findBy(array(), array('addDatetime' => 'DESC'));
    }
}