<?php

namespace Eglobal\AdvertBundle\Controller;

use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Eglobal\AdvertBundle\Entity\Advert;
use Eglobal\AdvertBundle\Form\AdvertType;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;


/**
 * Advert controller.
 *
 */
class AdvertController extends Controller
{
    private $todayAddLimits = array(
        'odd' => 1,
        'even'=> 2
    );

    private $weekAddLimit = 7;

    /**
     * Lists all Advert entities.
     *
     */

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AdvertBundle:Advert')->findAllAdverts();

        return $this->render('AdvertBundle:Advert:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Advert entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Advert();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $addLimits = $this -> getAdditionsRemains();
        if(!$addLimits['today'] || !$addLimits['week']){
            throw $this->createNotFoundException('Sorry, you can\'t add more advertisements today!');
        }
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity -> setOwner($this -> getCurrentUser());
            $entity -> setAddDatetime(new \DateTime());

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('advert_show', array('id' => $entity->getId())));
        }

        return $this->render('AdvertBundle:Advert:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Advert entity.
    *
    * @param Advert $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Advert $entity)
    {
        $form = $this->createForm(new AdvertType(), $entity, array(
            'action' => $this->generateUrl('advert_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Advert entity.
     *
     */
    public function newAction()
    {
        $entity = new Advert();
        $form = $this->createCreateForm($entity);

        $addLimits = $this -> getAdditionsRemains();

        return $this->render('AdvertBundle:Advert:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'addInfo'=> $addLimits,
        ));
    }

    /**
     * Finds and displays a Advert entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdvertBundle:Advert')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Advert entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdvertBundle:Advert:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Advert entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdvertBundle:Advert')->find($id);

        $this -> checkAdvertOwnerMatch($entity);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Advert entity.');
        }


        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdvertBundle:Advert:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
    * Creates a form to edit a Advert entity.
    *
    * @param Advert $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Advert $entity)
    {
        $form = $this->createForm(new AdvertType(), $entity, array(
            'action' => $this->generateUrl('advert_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Advert entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdvertBundle:Advert')->find($id);

        $this -> checkAdvertOwnerMatch($entity);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Advert entity.');
        }


        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $em->flush();

            return $this->redirect($this->generateUrl('advert_show', array('id' => $id)));
        }

        return $this->render('AdvertBundle:Advert:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Advert entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('AdvertBundle:Advert')->find($id);

            $this -> checkAdvertOwnerMatch($entity);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Advert entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('advert'));
    }

    /**
     * Creates a form to delete a Advert entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('advert_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    private function getCurrentUser(){
        return  $this->get('security.context')->getToken()->getUser();
    }

    private function checkAdvertOwnerMatch(Advert $ev){
        if($this -> getCurrentUser() != $ev -> getOwner()){
            throw new AccessDeniedException('Sorry, it is not your advertisement! ');
        }
    }

    private function getAdditionsRemains(){
        $curUser = $this -> getCurrentUser();
        //i don't use mysql function now() because server time and mysql server time could be different
        $serverTimeNow = date("Y-m-d H:i:s");
        $em = $this->getDoctrine()->getManager()
            ->getConnection()
            ->prepare("
                    (
                    SELECT count(id) as 'count', 'day' as 'period'
                    FROM advert
                    WHERE owner_id = '{$curUser -> getId()}'
                    AND add_datetime BETWEEN
                    DATE_FORMAT('$serverTimeNow','%Y-%m-%d 00:00:00')
                    AND
                    DATE_FORMAT('$serverTimeNow','%Y-%m-%d 23:59:59')
                    )
                    UNION
                    (SELECT count(id) as 'count', 'week' as 'period' FROM advert
                    WHERE owner_id = '{$curUser -> getId()}'
                    AND add_datetime > '$serverTimeNow' - INTERVAL 1 WEEK)");
        $em->execute();
        $result = $em->fetchAll();

        foreach($result as $limit){
            if($limit['period'] === 'day')
                $today = $this -> getTodayAddLimit() - $limit['count'];
            else
                $week = $this -> getWeekAddLimit() - $limit['count'];
        }

        $today=($today<0)?0:$today;
        if($week <= $today) $today = $week;
        $week=($week<0)?0:$week;

        return array('week'=>$week, 'today'=>$today);

    }

    private function getTodayAddLimit(){
        $curDayNumber = (int) date('z') + 1;
        $today = 'odd';
        if(($curDayNumber % 2) == 0){
            $today = 'even';
        }
        return $this -> todayAddLimits[$today];
    }

    private function getWeekAddLimit(){
        return $this -> weekAddLimit;
    }
}
