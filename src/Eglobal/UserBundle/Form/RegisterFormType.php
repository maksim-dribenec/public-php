<?php

namespace Eglobal\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class RegisterFormType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username','text',array('required'=>false))
            ->add('email','email',array('required'=>false))
            ->add('plainPassword','repeated', array(
                'type' => 'password',
                'invalid_message' => 'The password fields must match',
                'required' => false));
    }

    public function getDefaultOptions(array $options){
        return array(
            'data_class' => 'Klinklank\UserBundle\Entity\User'
        );
    }

    public function getName(){
       return 'user_register';
    }


}