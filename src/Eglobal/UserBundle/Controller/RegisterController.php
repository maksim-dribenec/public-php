<?php

namespace Eglobal\UserBundle\Controller;

use Eglobal\UserBundle\Form\RegisterFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;



//for @Template
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Eglobal\UserBundle\Entity\User;

class RegisterController extends Controller{
    /**
     * @Template()
     */
    public function registerAction(Request $request){

        $securityContext = $this->container->get('security.context');
        if( $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED') ){
            return new RedirectResponse($this->generateUrl('main_homepage'), 301);
        }

        $defaultData = array();
        $form = $this -> createFormBuilder($defaultData);

        $defaultUser = new User();

        //We create RegisterFormType file , so we can generate this form in any other controller - very reusable
        $form = $this -> createForm(new RegisterFormType(), $defaultUser);

        //Do not forgot to createView();

        if('POST' == $request->getMethod()){
            $form->submit($request);

            if($form -> isValid()){

                $user = $form->getData();
                $user -> setPassword($this->encodePassword($user , $user->getPlainPassword()));

                $em = $this -> getDoctrine() ->getManager();
                $em->persist($user);
                $em->flush();

                //Message about success registration
                $request -> getSession()->getFlashBag()->add(
                    'success',
                    'Registration was success'
                );
                //Redirect User
                $url = $this -> generateUrl('main_homepage');

                $this -> authenticateUser($user);

                return $this -> redirect($url);

            }
        }

        return array('regForm' => $form->createView());
    }

    private function encodePassword($user, $plainPassword){
        $encoder = $this -> container -> get('security.encoder_factory') -> getEncoder($user);
        return $encoder -> encodePassword($plainPassword, $user->getSalt());
    }

    //Manualy authenticate user ..... !!!!
    private function authenticateUser(UserInterface $user){
        $providerKey = 'secured_area'; //YOUR FIREWALL NAME
        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());
        $this -> container->get('security.context')->setToken($token);
    }
}
