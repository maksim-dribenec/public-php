<?php

namespace Eglobal\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Klinklank\UserBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadUsers implements FixtureInterface, ContainerAwareInterface{

    private $container;

    public function load(ObjectManager $manager){
        $user = new User();
        $user -> setUsername('user');
        $user -> setPassword($this->encodePassword($user, 'user'));
        $user -> setRoles(array('ROLE_USER'));
        $user -> setEmail('user@user.com');

        $manager -> persist($user);

        $admin = new User();
        $admin -> setUsername('admin');
        $admin -> setPassword($this->encodePassword($admin, 'admin'));
        $admin -> setRoles(array('ROLE_ADMIN'));
        $admin -> setEmail('admin@admin.com');

        $manager -> persist($admin);

        $manager -> flush();
    }

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
        $this -> container = $container;
    }

    private function encodePassword($user, $plainPassword){
       $encoder = $this -> container -> get('security.encoder_factory') -> getEncoder($user);
       return $encoder -> encodePassword($plainPassword, $user->getSalt());
    }

}