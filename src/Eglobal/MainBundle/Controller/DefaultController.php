<?php

namespace Eglobal\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $data = array();

        return $this->render('MainBundle:Pages:index.html.twig', $data);
    }

    public function test(){

    }
}
