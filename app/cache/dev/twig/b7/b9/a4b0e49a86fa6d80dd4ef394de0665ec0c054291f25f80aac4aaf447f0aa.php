<?php

/* AdvertBundle:Advert:index.html.twig */
class __TwigTemplate_b7b9a4b0e49a86fa6d80dd4ef394de0665ec0c054291f25f80aac4aaf447f0aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Advertisements</h1>

    ";
        // line 6
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 7
            echo "         ";
            $context["borderColor"] = "#f9ffad";
            // line 8
            echo "         ";
            $context["myAdvert"] = false;
            // line 9
            echo "        
         ";
            // line 10
            if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
                // line 11
                echo "            ";
                if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "owner") == $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"))) {
                    // line 12
                    echo "                ";
                    $context["borderColor"] = "#FF7B42";
                    // line 13
                    echo "                ";
                    $context["myAdvert"] = true;
                    // line 14
                    echo "            ";
                }
                // line 15
                echo "         ";
            }
            // line 16
            echo "        <div class=\"form-horizontal\" style=\"border:1px solid ";
            echo twig_escape_filter($this->env, (isset($context["borderColor"]) ? $context["borderColor"] : $this->getContext($context, "borderColor")), "html", null, true);
            echo "; padding: 10px;background-color: #fdffe2; margin: 5px 0;\">
            <div class=\"form-group\">
                <div class=\"col-sm-10\">
                    <p class=\"form-control-static\">";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "title"), "html", null, true);
            echo "</p>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"col-sm-10\">
                    <p class=\"form-control-static\" style=\" border-bottom: 1px dotted #ffa36a;\">";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "text"), "html", null, true);
            echo "</p>
                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\">Create Date:</label>
                <div class=\"col-sm-10\">
                    <p class=\"form-control-static\">";
            // line 30
            if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "addDatetime")) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "addDatetime"), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</p>
                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\">Owner:</label>
                <div class=\"col-sm-10\">
                    <p class=\"form-control-static\">";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "owner"), "username"), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "owner"), "email"), "html", null, true);
            echo ")</p>
                </div>
            </div>
            ";
            // line 42
            echo "        </div>
        <a class=\"btn btn-default\" href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("advert_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\" role=\"button\">Preview</a>

        ";
            // line 45
            if ((isset($context["myAdvert"]) ? $context["myAdvert"] : $this->getContext($context, "myAdvert"))) {
                // line 46
                echo "            <a class=\"btn btn-default\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("advert_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" role=\"button\">edit</a>
        ";
            }
            // line 48
            echo "
        <hr>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "
";
    }

    public function getTemplateName()
    {
        return "AdvertBundle:Advert:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 52,  124 => 48,  118 => 46,  116 => 45,  111 => 43,  108 => 42,  100 => 36,  89 => 30,  80 => 24,  72 => 19,  65 => 16,  62 => 15,  59 => 14,  56 => 13,  53 => 12,  50 => 11,  48 => 10,  45 => 9,  42 => 8,  39 => 7,  35 => 6,  31 => 4,  28 => 3,);
    }
}
