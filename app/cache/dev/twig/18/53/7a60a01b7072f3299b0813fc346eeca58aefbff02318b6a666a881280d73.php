<?php

/* AdvertBundle:Advert:edit.html.twig */
class __TwigTemplate_18537a60a01b7072f3299b0813fc346eeca58aefbff02318b6a666a881280d73 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_javascripts($context, array $blocks = array())
    {
        // line 3
        echo "     ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
     <script type=\"text/javascript\" src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/script.js"), "html", null, true);
        echo "\"></script>
 ";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "<h1>Edit Advertisement</h1>

    ";
        // line 10
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start', array("attr" => array("class" => "form-horizontal")));
        echo "
    <div class=\"form-group\">
        <label for=\"inputTitle\" class=\"col-sm-2 control-label\">Title</label>
        <div class=\"col-sm-10\">
            ";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "title"), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Advertisement title")));
        echo "
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Text</label>
        <div class=\"col-sm-10\">
            ";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "text"), 'widget', array("attr" => array("class" => "form-control", "rows" => 3, "placeholder" => "Advertisement text")));
        echo "
        </div>
    </div>

    <div class=\"form-group\">
        <div class=\"col-sm-offset-2 col-sm-10\">
            ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "submit"), 'row', array("attr" => array("class" => "btn btn-primary btn-lg btn-block")));
        echo "
            ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'rest', array("attr" => array("class" => "btn btn-default")));
        echo "
        </div>
    </div>
    <div class=\"form-group\" style=\"text-align: center;\">
        <a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("main_homepage");
        echo "\">Back to the list</a>
    </div>
    ";
        // line 33
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "

    ";
        // line 35
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start', array("attr" => array("style" => "text-align:center;")));
        echo "
        ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), "submit"), 'row', array("label" => "Delete This Advertisement", "attr" => array("class" => "\"btn btn-danger"), "id" => "deleteAdvert"));
        echo "
        ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'rest');
        echo "
    ";
        // line 38
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "

";
    }

    public function getTemplateName()
    {
        return "AdvertBundle:Advert:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 38,  104 => 37,  100 => 36,  96 => 35,  91 => 33,  86 => 31,  79 => 27,  75 => 26,  66 => 20,  57 => 14,  50 => 10,  46 => 8,  43 => 6,  37 => 4,  32 => 3,  29 => 2,);
    }
}
