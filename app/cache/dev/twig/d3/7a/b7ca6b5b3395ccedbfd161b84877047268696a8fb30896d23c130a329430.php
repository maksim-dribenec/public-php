<?php

/* AdvertBundle:Advert:show.html.twig */
class __TwigTemplate_d37ab7ca6b5b3395ccedbfd161b84877047268696a8fb30896d23c130a329430 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_javascripts($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\" src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/script.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "<h1>Advertisement Preview</h1>
<div class=\"form-horizontal\">
    <div class=\"form-group\">
        <label for=\"inputTitle\" class=\"col-sm-2 control-label\" >Title</label>
        <div class=\"col-sm-10\">
            <p class=\"form-control-static\">";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "title"), "html", null, true);
        echo "</p>
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"inputTitle\" class=\"col-sm-2 control-label\" >Text</label>
        <div class=\"col-sm-10\">
            <p class=\"form-control-static\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "text"), "html", null, true);
        echo "</p>
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"inputTitle\" class=\"col-sm-2 control-label\" >Created</label>
        <div class=\"col-sm-10\">
            <p class=\"form-control-static\">";
        // line 24
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "addDatetime"), "Y-m-d H:i:s"), "html", null, true);
        echo "</p>
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"inputTitle\" class=\"col-sm-2 control-label\" >Owner Info</label>
        <div class=\"col-sm-10\">
            <p class=\"form-control-static\">";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "owner"), "username"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "owner"), "email"), "html", null, true);
        echo "</p>
        </div>
    </div>

    ";
        // line 34
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 35
            echo "        ";
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "owner") == $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"))) {
                // line 36
                echo "            <div style=\"width:100%; text-align: center; margin:10px 0;\">
                <a href=\"";
                // line 37
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("advert_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" class=\"btn btn-primary btn-lg btn-block\" role=\"button\">Edit</a>
            </div>
        ";
            }
            // line 40
            echo "    ";
        }
        // line 41
        echo "
    <div class=\"form-group\" style=\"text-align: center;\">
        <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("main_homepage");
        echo "\">
            Back to the list
        </a>
    </div>

    ";
        // line 48
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 49
            echo "        ";
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "owner") == $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"))) {
                // line 50
                echo "            ";
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start', array("attr" => array("style" => "text-align:center;")));
                echo "
            ";
                // line 51
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), "submit"), 'row', array("label" => "Delete This Advertisement", "attr" => array("class" => "\"btn btn-danger"), "id" => "deleteAdvert"));
                echo "
            ";
                // line 52
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'rest');
                echo "
            ";
                // line 53
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
                echo "
        ";
            }
            // line 55
            echo "    ";
        }
        // line 56
        echo "
</div>




";
    }

    public function getTemplateName()
    {
        return "AdvertBundle:Advert:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 56,  141 => 55,  136 => 53,  132 => 52,  128 => 51,  123 => 50,  120 => 49,  118 => 48,  110 => 43,  106 => 41,  103 => 40,  97 => 37,  94 => 36,  91 => 35,  89 => 34,  80 => 30,  71 => 24,  62 => 18,  53 => 12,  46 => 7,  43 => 6,  37 => 4,  32 => 3,  29 => 2,);
    }
}
