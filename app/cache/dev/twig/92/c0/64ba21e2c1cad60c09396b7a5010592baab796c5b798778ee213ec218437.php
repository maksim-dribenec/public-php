<?php

/* UserBundle:Register:register.html.twig */
class __TwigTemplate_92c064ba21e2c1cad60c09396b7a5010592baab796c5b798778ee213ec218437 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "        <h1>Registration Form</h1>
        <form class=\"form-horizontal\" role=\"form\" action=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("register_page");
        echo "\" method=\"post\">
            <div class=\"form-group\">
                ";
        // line 8
        echo "                ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["regForm"]) ? $context["regForm"] : $this->getContext($context, "regForm")), "username"), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Username"));
        echo "
                <div class=\"col-sm-10\">
                    ";
        // line 11
        echo "                    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["regForm"]) ? $context["regForm"] : $this->getContext($context, "regForm")), "username"), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Username")));
        echo "
                    ";
        // line 12
        if ($this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["regForm"]) ? $context["regForm"] : $this->getContext($context, "regForm")), "username"), 'errors')) {
            // line 13
            echo "                        <span class=\"help-block\" style=\"color:#ff0000\">";
            echo twig_escape_filter($this->env, strip_tags($this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["regForm"]) ? $context["regForm"] : $this->getContext($context, "regForm")), "username"), 'errors')), "html", null, true);
            echo "</span>
                    ";
        }
        // line 15
        echo "                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\">Email</label>
                <div class=\"col-sm-10\">
                    ";
        // line 21
        echo "                    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["regForm"]) ? $context["regForm"] : $this->getContext($context, "regForm")), "email"), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Email")));
        echo "
                    ";
        // line 22
        if ($this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["regForm"]) ? $context["regForm"] : $this->getContext($context, "regForm")), "email"), 'errors')) {
            // line 23
            echo "                        <span class=\"help-block\" style=\"color:#ff0000\">";
            echo twig_escape_filter($this->env, strip_tags($this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["regForm"]) ? $context["regForm"] : $this->getContext($context, "regForm")), "email"), 'errors')), "html", null, true);
            echo "</span>
                    ";
        }
        // line 25
        echo "                </div>

            </div>
            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\">Password</label>
                <div class=\"col-sm-10\">
                    ";
        // line 32
        echo "                    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["regForm"]) ? $context["regForm"] : $this->getContext($context, "regForm")), "plainPassword"), "first"), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "password")));
        echo "

                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\">Confirm Password</label>
                <div class=\"col-sm-10\">
                    ";
        // line 40
        echo "                    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["regForm"]) ? $context["regForm"] : $this->getContext($context, "regForm")), "plainPassword"), "second"), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Configm password")));
        echo "
                    ";
        // line 41
        if ($this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["regForm"]) ? $context["regForm"] : $this->getContext($context, "regForm")), "plainPassword"), "first"), 'errors')) {
            // line 42
            echo "                        <span class=\"help-block\" style=\"color:#ff0000\">";
            echo twig_escape_filter($this->env, strip_tags($this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["regForm"]) ? $context["regForm"] : $this->getContext($context, "regForm")), "plainPassword"), "first"), 'errors')), "html", null, true);
            echo "</span>
                    ";
        }
        // line 44
        echo "                </div>
            </div>

            ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["regForm"]) ? $context["regForm"] : $this->getContext($context, "regForm")), 'rest');
        echo "

            <div class=\"form-group\">
                <div class=\"col-sm-offset-2 col-sm-10\">
                    <input type=\"submit\" formnovalidate class=\"btn btn-primary btn-lg btn-block\" value=\"Register\">
                </div>
            </div>
        </form>


";
    }

    public function getTemplateName()
    {
        return "UserBundle:Register:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 47,  110 => 44,  104 => 42,  102 => 41,  97 => 40,  86 => 32,  78 => 25,  72 => 23,  70 => 22,  65 => 21,  58 => 15,  52 => 13,  50 => 12,  45 => 11,  39 => 8,  34 => 5,  31 => 4,  28 => 3,);
    }
}
