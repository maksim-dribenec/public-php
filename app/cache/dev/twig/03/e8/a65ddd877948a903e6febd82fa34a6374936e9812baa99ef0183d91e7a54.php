<?php

/* AdvertBundle:Advert:new.html.twig */
class __TwigTemplate_03e8a65ddd877948a903e6febd82fa34a6374936e9812baa99ef0183d91e7a54 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Create Advertisement</h1>

    <form class=\"form-horizontal\" role=\"form\"  method=\"post\" action=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("advert_create");
        echo "\">
        <div class=\"form-group\">
            <label for=\"inputTitle\" class=\"col-sm-2 control-label\" >Additions Remains</label>
            <div class=\"col-sm-10\">
                <p class=\"form-control-static\">This Week : ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addInfo"]) ? $context["addInfo"] : $this->getContext($context, "addInfo")), "week"), "html", null, true);
        echo " , Today : ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addInfo"]) ? $context["addInfo"] : $this->getContext($context, "addInfo")), "today"), "html", null, true);
        echo "</p>
            </div>
        </div>
        <div class=\"form-group\">
            <label for=\"inputTitle\" class=\"col-sm-2 control-label\">Title</label>
            <div class=\"col-sm-10\">
                ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "title"), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Advertisement title")));
        echo "
            </div>
        </div>
        <div class=\"form-group\">
            <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Text</label>
            <div class=\"col-sm-10\">
                ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "text"), 'widget', array("attr" => array("class" => "form-control", "rows" => 3, "placeholder" => "Advertisement text")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            <div class=\"col-sm-offset-2 col-sm-10\">
                ";
        // line 28
        if (($this->getAttribute((isset($context["addInfo"]) ? $context["addInfo"] : $this->getContext($context, "addInfo")), "week") && $this->getAttribute((isset($context["addInfo"]) ? $context["addInfo"] : $this->getContext($context, "addInfo")), "today"))) {
            // line 29
            echo "                    ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit"), 'row', array("attr" => array("class" => "btn btn-primary btn-lg btn-block")));
            echo "
                ";
        } else {
            // line 31
            echo "                    ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit"), 'row', array("attr" => array("class" => "btn btn-primary btn-lg active", "disabled" => "disabled")));
            echo "
                ";
        }
        // line 33
        echo "
                ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
             </div>
        </div>
        <div class=\"form-group\" style=\"text-align: center;\">
            <a href=\"";
        // line 38
        echo $this->env->getExtension('routing')->getPath("main_homepage");
        echo "\">
                Back to the list
            </a>
        </div>
    </form>




";
    }

    public function getTemplateName()
    {
        return "AdvertBundle:Advert:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 38,  88 => 34,  85 => 33,  79 => 31,  73 => 29,  71 => 28,  62 => 22,  53 => 16,  42 => 10,  35 => 6,  31 => 4,  28 => 3,);
    }
}
