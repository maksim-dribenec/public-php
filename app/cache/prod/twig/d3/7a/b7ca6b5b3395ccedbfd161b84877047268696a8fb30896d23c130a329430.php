<?php

/* AdvertBundle:Advert:show.html.twig */
class __TwigTemplate_d37ab7ca6b5b3395ccedbfd161b84877047268696a8fb30896d23c130a329430 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_javascripts($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\" src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/script.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "<h1>Advertisement Preview</h1>
<div class=\"form-horizontal\">
    <div class=\"form-group\">
        <label for=\"inputTitle\" class=\"col-sm-2 control-label\" >Title</label>
        <div class=\"col-sm-10\">
            <p class=\"form-control-static\">";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "title"), "html", null, true);
        echo "</p>
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"inputTitle\" class=\"col-sm-2 control-label\" >Text</label>
        <div class=\"col-sm-10\">
            <p class=\"form-control-static\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "text"), "html", null, true);
        echo "</p>
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"inputTitle\" class=\"col-sm-2 control-label\" >Created</label>
        <div class=\"col-sm-10\">
            <p class=\"form-control-static\">";
        // line 24
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "addDatetime"), "Y-m-d H:i:s"), "html", null, true);
        echo "</p>
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"inputTitle\" class=\"col-sm-2 control-label\" >Owner Info</label>
        <div class=\"col-sm-10\">
            <p class=\"form-control-static\">";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "owner"), "username"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "owner"), "email"), "html", null, true);
        echo "</p>
        </div>
    </div>

    ";
        // line 34
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 35
            echo "        ";
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "owner") == $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"))) {
                // line 36
                echo "            <div style=\"width:100%; text-align: center; margin:10px 0;\">
                <a href=\"";
                // line 37
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("advert_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
                echo "\" class=\"btn btn-primary btn-lg btn-block\" role=\"button\">Edit</a>
            </div>
        ";
            }
            // line 40
            echo "    ";
        }
        // line 41
        echo "
    <div class=\"form-group\" style=\"text-align: center;\">
        <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("main_homepage");
        echo "\">
            Back to the list
        </a>
    </div>

    ";
        // line 48
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 49
            echo "        ";
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "owner") == $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"))) {
                // line 50
                echo "            ";
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_start', array("attr" => array("style" => "text-align:center;")));
                echo "
            ";
                // line 51
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["delete_form"]) ? $context["delete_form"] : null), "submit"), 'row', array("label" => "Delete This Advertisement", "attr" => array("class" => "\"btn btn-danger"), "id" => "deleteAdvert"));
                echo "
            ";
                // line 52
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'rest');
                echo "
            ";
                // line 53
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_end');
                echo "
        ";
            }
            // line 55
            echo "    ";
        }
        // line 56
        echo "
</div>




";
    }

    public function getTemplateName()
    {
        return "AdvertBundle:Advert:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 49,  124 => 48,  100 => 36,  37 => 4,  110 => 43,  104 => 37,  97 => 37,  70 => 22,  65 => 16,  58 => 15,  52 => 13,  34 => 5,  53 => 12,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 55,  132 => 52,  128 => 51,  107 => 36,  273 => 96,  269 => 94,  254 => 92,  246 => 90,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 61,  143 => 56,  135 => 53,  131 => 52,  119 => 42,  102 => 41,  67 => 15,  63 => 15,  59 => 14,  38 => 6,  94 => 36,  89 => 34,  85 => 33,  79 => 31,  75 => 26,  68 => 14,  50 => 11,  87 => 25,  72 => 19,  55 => 20,  21 => 2,  26 => 6,  98 => 31,  93 => 28,  88 => 34,  78 => 25,  46 => 7,  27 => 4,  44 => 12,  43 => 6,  41 => 7,  201 => 92,  196 => 90,  183 => 82,  171 => 61,  166 => 71,  163 => 62,  158 => 67,  156 => 66,  151 => 63,  142 => 59,  138 => 54,  136 => 53,  123 => 50,  117 => 44,  115 => 47,  105 => 40,  101 => 32,  91 => 35,  69 => 25,  62 => 18,  49 => 19,  32 => 3,  25 => 3,  24 => 4,  121 => 46,  118 => 48,  111 => 43,  108 => 42,  96 => 35,  90 => 5,  83 => 34,  81 => 32,  76 => 29,  71 => 24,  66 => 20,  61 => 22,  56 => 13,  47 => 9,  40 => 8,  35 => 6,  29 => 2,  23 => 1,  31 => 4,  28 => 3,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 72,  164 => 59,  162 => 59,  154 => 58,  149 => 51,  147 => 58,  144 => 56,  141 => 55,  133 => 52,  130 => 41,  125 => 44,  122 => 43,  116 => 45,  112 => 42,  109 => 34,  106 => 41,  103 => 40,  99 => 31,  95 => 38,  92 => 21,  86 => 31,  82 => 22,  80 => 30,  73 => 29,  64 => 17,  60 => 13,  57 => 14,  54 => 10,  51 => 14,  48 => 10,  45 => 9,  42 => 10,  39 => 7,  36 => 5,  33 => 4,  30 => 7,);
    }
}
