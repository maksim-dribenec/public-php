<?php

/* ::base.html.twig */
class __TwigTemplate_cf7c4b9d90da5e4aa551e116cdbf4db806518974faaf4576cfc4613362d52b1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\" />
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 13
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    ";
        // line 14
        $this->displayBlock('javascripts', $context, $blocks);
        // line 17
        echo "</head>
<body style=\"width:960px; margin:auto;\">
<header style=\"width:100%; height:60px; background-color: #ddd; margin-bottom:20px; padding:5px;\">
    <a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("main_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/eglobal.jpg"), "html", null, true);
        echo "\" height=\"50px\"/></a>

    ";
        // line 22
        if (($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user") && $this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED"))) {
            // line 23
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("logout");
            echo "\" class=\"btn btn-primary btn-lg\" role=\"button\" style=\"float:right;margin:0 10px\">Logout</a>
        <a href=\"";
            // line 24
            echo $this->env->getExtension('routing')->getPath("advert_new");
            echo "\" class=\"btn btn-primary btn-lg\" role=\"button\" style=\"float:right;\">Add Advertisements</a>
    ";
        } else {
            // line 26
            echo "        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("register_page");
            echo "\" class=\"btn btn-primary btn-lg\" role=\"button\" style=\"float:right;margin:0 10px; \">Registration</a>
        <a href=\"";
            // line 27
            echo $this->env->getExtension('routing')->getPath("login");
            echo "\" class=\"btn btn-primary btn-lg\" role=\"button\" style=\"float:right;\">Login</a>
    ";
        }
        // line 29
        echo "
</header>
<div style=\"width:900px; margin: auto; padding:15px\" >
    ";
        // line 32
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "</div>

</body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "advertisements";
    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 7
        echo "        <!-- Latest compiled and minified CSS -->
        <link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css\">

        <!-- Optional theme -->
        <link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap-theme.min.css\">
    ";
    }

    // line 14
    public function block_javascripts($context, array $blocks = array())
    {
        // line 15
        echo "        <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    // line 32
    public function block_body($context, array $blocks = array())
    {
        // line 33
        echo "    ";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 33,  118 => 32,  111 => 15,  108 => 14,  96 => 6,  90 => 5,  83 => 34,  81 => 32,  76 => 29,  71 => 27,  66 => 26,  61 => 24,  56 => 23,  47 => 20,  40 => 14,  35 => 13,  29 => 5,  23 => 1,  31 => 4,  28 => 3,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 76,  193 => 73,  189 => 71,  187 => 70,  182 => 68,  176 => 64,  173 => 63,  168 => 62,  164 => 60,  162 => 59,  154 => 54,  149 => 51,  147 => 50,  144 => 49,  141 => 48,  133 => 42,  130 => 41,  125 => 38,  122 => 37,  116 => 36,  112 => 35,  109 => 34,  106 => 33,  103 => 32,  99 => 7,  95 => 28,  92 => 27,  86 => 24,  82 => 22,  80 => 21,  73 => 19,  64 => 15,  60 => 13,  57 => 12,  54 => 22,  51 => 10,  48 => 9,  45 => 8,  42 => 17,  39 => 6,  36 => 5,  33 => 6,  30 => 3,);
    }
}
