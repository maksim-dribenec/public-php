<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_30202670ad41ab160c45c3c869203b8137107ac84daf6b81d7460749ca2b0122 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("TwigBundle:Exception:exception.xml.twig")->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : null))));
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 3,  24 => 4,  121 => 33,  118 => 32,  111 => 15,  108 => 14,  96 => 6,  90 => 5,  83 => 34,  81 => 32,  76 => 29,  71 => 27,  66 => 26,  61 => 24,  56 => 23,  47 => 20,  40 => 14,  35 => 13,  29 => 5,  23 => 1,  31 => 4,  28 => 3,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 76,  193 => 73,  189 => 71,  187 => 70,  182 => 68,  176 => 64,  173 => 63,  168 => 62,  164 => 60,  162 => 59,  154 => 54,  149 => 51,  147 => 50,  144 => 49,  141 => 48,  133 => 42,  130 => 41,  125 => 38,  122 => 37,  116 => 36,  112 => 35,  109 => 34,  106 => 33,  103 => 32,  99 => 7,  95 => 28,  92 => 27,  86 => 24,  82 => 22,  80 => 21,  73 => 19,  64 => 15,  60 => 13,  57 => 12,  54 => 22,  51 => 10,  48 => 9,  45 => 8,  42 => 17,  39 => 6,  36 => 5,  33 => 6,  30 => 3,);
    }
}
