<?php

/* AdvertBundle:Advert:new.html.twig */
class __TwigTemplate_03e8a65ddd877948a903e6febd82fa34a6374936e9812baa99ef0183d91e7a54 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Create Advertisement</h1>

    <form class=\"form-horizontal\" role=\"form\"  method=\"post\" action=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("advert_create");
        echo "\">
        <div class=\"form-group\">
            <label for=\"inputTitle\" class=\"col-sm-2 control-label\" >Additions Remains</label>
            <div class=\"col-sm-10\">
                <p class=\"form-control-static\">This Week : ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addInfo"]) ? $context["addInfo"] : null), "week"), "html", null, true);
        echo " , Today : ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["addInfo"]) ? $context["addInfo"] : null), "today"), "html", null, true);
        echo "</p>
            </div>
        </div>
        <div class=\"form-group\">
            <label for=\"inputTitle\" class=\"col-sm-2 control-label\">Title</label>
            <div class=\"col-sm-10\">
                ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "title"), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Advertisement title")));
        echo "
            </div>
        </div>
        <div class=\"form-group\">
            <label for=\"inputPassword3\" class=\"col-sm-2 control-label\">Text</label>
            <div class=\"col-sm-10\">
                ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "text"), 'widget', array("attr" => array("class" => "form-control", "rows" => 3, "placeholder" => "Advertisement text")));
        echo "
            </div>
        </div>

        <div class=\"form-group\">
            <div class=\"col-sm-offset-2 col-sm-10\">
                ";
        // line 28
        if (($this->getAttribute((isset($context["addInfo"]) ? $context["addInfo"] : null), "week") && $this->getAttribute((isset($context["addInfo"]) ? $context["addInfo"] : null), "today"))) {
            // line 29
            echo "                    ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "submit"), 'row', array("attr" => array("class" => "btn btn-primary btn-lg btn-block")));
            echo "
                ";
        } else {
            // line 31
            echo "                    ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "submit"), 'row', array("attr" => array("class" => "btn btn-primary btn-lg active", "disabled" => "disabled")));
            echo "
                ";
        }
        // line 33
        echo "
                ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
             </div>
        </div>
        <div class=\"form-group\" style=\"text-align: center;\">
            <a href=\"";
        // line 38
        echo $this->env->getExtension('routing')->getPath("main_homepage");
        echo "\">
                Back to the list
            </a>
        </div>
    </form>




";
    }

    public function getTemplateName()
    {
        return "AdvertBundle:Advert:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 48,  100 => 36,  37 => 4,  110 => 44,  104 => 37,  97 => 40,  70 => 22,  65 => 16,  58 => 15,  52 => 13,  34 => 5,  53 => 16,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 55,  132 => 51,  128 => 49,  107 => 36,  273 => 96,  269 => 94,  254 => 92,  246 => 90,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 61,  143 => 56,  135 => 53,  131 => 52,  119 => 42,  102 => 41,  67 => 15,  63 => 15,  59 => 14,  38 => 6,  94 => 28,  89 => 30,  85 => 33,  79 => 31,  75 => 26,  68 => 14,  50 => 11,  87 => 25,  72 => 19,  55 => 20,  21 => 2,  26 => 6,  98 => 31,  93 => 28,  88 => 34,  78 => 25,  46 => 8,  27 => 4,  44 => 12,  43 => 6,  41 => 7,  201 => 92,  196 => 90,  183 => 82,  171 => 61,  166 => 71,  163 => 62,  158 => 67,  156 => 66,  151 => 63,  142 => 59,  138 => 54,  136 => 56,  123 => 47,  117 => 44,  115 => 47,  105 => 40,  101 => 32,  91 => 33,  69 => 25,  62 => 22,  49 => 19,  32 => 3,  25 => 3,  24 => 4,  121 => 46,  118 => 46,  111 => 43,  108 => 42,  96 => 35,  90 => 5,  83 => 34,  81 => 32,  76 => 29,  71 => 28,  66 => 20,  61 => 22,  56 => 13,  47 => 9,  40 => 8,  35 => 6,  29 => 2,  23 => 1,  31 => 4,  28 => 3,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 72,  164 => 59,  162 => 59,  154 => 58,  149 => 51,  147 => 58,  144 => 49,  141 => 48,  133 => 52,  130 => 41,  125 => 44,  122 => 43,  116 => 45,  112 => 42,  109 => 34,  106 => 33,  103 => 32,  99 => 31,  95 => 38,  92 => 21,  86 => 31,  82 => 22,  80 => 24,  73 => 29,  64 => 17,  60 => 13,  57 => 14,  54 => 10,  51 => 14,  48 => 10,  45 => 9,  42 => 10,  39 => 7,  36 => 5,  33 => 4,  30 => 7,);
    }
}
