<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_ccdf19dcbea584446fd865adee0053072c884e09c330191217e88c33f2177b8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_jsonencode_filter($this->getAttribute((isset($context["exception"]) ? $context["exception"] : null), "toarray"));
        echo "
";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 92,  196 => 90,  183 => 82,  171 => 73,  166 => 71,  163 => 70,  158 => 67,  156 => 66,  151 => 63,  142 => 59,  138 => 57,  136 => 56,  123 => 47,  117 => 44,  115 => 43,  105 => 40,  101 => 39,  91 => 31,  69 => 25,  62 => 23,  49 => 19,  32 => 12,  25 => 3,  24 => 3,  121 => 46,  118 => 32,  111 => 15,  108 => 14,  96 => 6,  90 => 5,  83 => 34,  81 => 32,  76 => 29,  71 => 27,  66 => 24,  61 => 24,  56 => 23,  47 => 20,  40 => 14,  35 => 13,  29 => 5,  23 => 1,  31 => 4,  28 => 3,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 91,  193 => 73,  189 => 71,  187 => 84,  182 => 68,  176 => 64,  173 => 74,  168 => 72,  164 => 60,  162 => 59,  154 => 54,  149 => 51,  147 => 50,  144 => 49,  141 => 48,  133 => 55,  130 => 41,  125 => 38,  122 => 37,  116 => 36,  112 => 42,  109 => 34,  106 => 33,  103 => 32,  99 => 7,  95 => 28,  92 => 27,  86 => 28,  82 => 22,  80 => 21,  73 => 19,  64 => 15,  60 => 13,  57 => 12,  54 => 21,  51 => 20,  48 => 9,  45 => 8,  42 => 17,  39 => 16,  36 => 5,  33 => 6,  30 => 3,);
    }
}
