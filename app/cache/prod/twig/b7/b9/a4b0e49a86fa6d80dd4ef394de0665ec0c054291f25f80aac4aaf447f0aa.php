<?php

/* AdvertBundle:Advert:index.html.twig */
class __TwigTemplate_b7b9a4b0e49a86fa6d80dd4ef394de0665ec0c054291f25f80aac4aaf447f0aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Advertisements</h1>

    ";
        // line 6
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 7
            echo "         ";
            $context["borderColor"] = "#f9ffad";
            // line 8
            echo "         ";
            $context["myAdvert"] = false;
            // line 9
            echo "        
         ";
            // line 10
            if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
                // line 11
                echo "            ";
                if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "owner") == $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"))) {
                    // line 12
                    echo "                ";
                    $context["borderColor"] = "#FF7B42";
                    // line 13
                    echo "                ";
                    $context["myAdvert"] = true;
                    // line 14
                    echo "            ";
                }
                // line 15
                echo "         ";
            }
            // line 16
            echo "        <div class=\"form-horizontal\" style=\"border:1px solid ";
            echo twig_escape_filter($this->env, (isset($context["borderColor"]) ? $context["borderColor"] : null), "html", null, true);
            echo "; padding: 10px;background-color: #fdffe2; margin: 5px 0;\">
            <div class=\"form-group\">
                <div class=\"col-sm-10\">
                    <p class=\"form-control-static\">";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "title"), "html", null, true);
            echo "</p>
                </div>
            </div>
            <div class=\"form-group\">
                <div class=\"col-sm-10\">
                    <p class=\"form-control-static\" style=\" border-bottom: 1px dotted #ffa36a;\">";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "text"), "html", null, true);
            echo "</p>
                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\">Create Date:</label>
                <div class=\"col-sm-10\">
                    <p class=\"form-control-static\">";
            // line 30
            if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "addDatetime")) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "addDatetime"), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</p>
                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\">Owner:</label>
                <div class=\"col-sm-10\">
                    <p class=\"form-control-static\">";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "owner"), "username"), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "owner"), "email"), "html", null, true);
            echo ")</p>
                </div>
            </div>
            ";
            // line 42
            echo "        </div>
        <a class=\"btn btn-default\" href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("advert_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
            echo "\" role=\"button\">Preview</a>

        ";
            // line 45
            if ((isset($context["myAdvert"]) ? $context["myAdvert"] : null)) {
                // line 46
                echo "            <a class=\"btn btn-default\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("advert_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
                echo "\" role=\"button\">edit</a>
        ";
            }
            // line 48
            echo "
        <hr>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "
";
    }

    public function getTemplateName()
    {
        return "AdvertBundle:Advert:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 48,  100 => 36,  37 => 4,  110 => 44,  104 => 37,  97 => 40,  70 => 22,  65 => 16,  58 => 15,  52 => 13,  34 => 5,  53 => 12,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 55,  132 => 51,  128 => 49,  107 => 36,  273 => 96,  269 => 94,  254 => 92,  246 => 90,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 61,  143 => 56,  135 => 53,  131 => 52,  119 => 42,  102 => 41,  67 => 15,  63 => 15,  59 => 14,  38 => 6,  94 => 28,  89 => 30,  85 => 25,  79 => 27,  75 => 26,  68 => 14,  50 => 11,  87 => 25,  72 => 19,  55 => 20,  21 => 2,  26 => 6,  98 => 31,  93 => 28,  88 => 6,  78 => 25,  46 => 8,  27 => 4,  44 => 12,  43 => 6,  41 => 7,  201 => 92,  196 => 90,  183 => 82,  171 => 61,  166 => 71,  163 => 62,  158 => 67,  156 => 66,  151 => 63,  142 => 59,  138 => 54,  136 => 56,  123 => 47,  117 => 44,  115 => 47,  105 => 40,  101 => 32,  91 => 33,  69 => 25,  62 => 15,  49 => 19,  32 => 3,  25 => 3,  24 => 4,  121 => 46,  118 => 46,  111 => 43,  108 => 42,  96 => 35,  90 => 5,  83 => 34,  81 => 32,  76 => 29,  71 => 19,  66 => 20,  61 => 22,  56 => 13,  47 => 9,  40 => 8,  35 => 6,  29 => 2,  23 => 1,  31 => 4,  28 => 3,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 72,  164 => 59,  162 => 59,  154 => 58,  149 => 51,  147 => 58,  144 => 49,  141 => 48,  133 => 52,  130 => 41,  125 => 44,  122 => 43,  116 => 45,  112 => 42,  109 => 34,  106 => 33,  103 => 32,  99 => 31,  95 => 28,  92 => 21,  86 => 31,  82 => 22,  80 => 24,  73 => 19,  64 => 17,  60 => 13,  57 => 14,  54 => 10,  51 => 14,  48 => 10,  45 => 9,  42 => 8,  39 => 7,  36 => 5,  33 => 4,  30 => 7,);
    }
}
