<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        // advert_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'advert_homepage')), array (  '_controller' => 'Eglobal\\AdvertBundle\\Controller\\DefaultController::indexAction',));
        }

        if (0 === strpos($pathinfo, '/advert')) {
            // advert
            if (rtrim($pathinfo, '/') === '/advert') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'advert');
                }

                return array (  '_controller' => 'Eglobal\\AdvertBundle\\Controller\\AdvertController::indexAction',  '_route' => 'advert',);
            }

            // advert_show
            if (preg_match('#^/advert/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'advert_show')), array (  '_controller' => 'Eglobal\\AdvertBundle\\Controller\\AdvertController::showAction',));
            }

            // advert_new
            if ($pathinfo === '/advert/new') {
                return array (  '_controller' => 'Eglobal\\AdvertBundle\\Controller\\AdvertController::newAction',  '_route' => 'advert_new',);
            }

            // advert_create
            if ($pathinfo === '/advert/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_advert_create;
                }

                return array (  '_controller' => 'Eglobal\\AdvertBundle\\Controller\\AdvertController::createAction',  '_route' => 'advert_create',);
            }
            not_advert_create:

            // advert_edit
            if (preg_match('#^/advert/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'advert_edit')), array (  '_controller' => 'Eglobal\\AdvertBundle\\Controller\\AdvertController::editAction',));
            }

            // advert_update
            if (preg_match('#^/advert/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_advert_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'advert_update')), array (  '_controller' => 'Eglobal\\AdvertBundle\\Controller\\AdvertController::updateAction',));
            }
            not_advert_update:

            // advert_delete
            if (preg_match('#^/advert/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_advert_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'advert_delete')), array (  '_controller' => 'Eglobal\\AdvertBundle\\Controller\\AdvertController::deleteAction',));
            }
            not_advert_delete:

        }

        // main_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'main_homepage');
            }

            return array (  '_controller' => 'Eglobal\\AdvertBundle\\Controller\\AdvertController::indexAction',  '_route' => 'main_homepage',);
        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // login
                if ($pathinfo === '/login') {
                    return array (  '_controller' => 'Eglobal\\UserBundle\\Controller\\LoginController::loginAction',  '_route' => 'login',);
                }

                // login_check
                if ($pathinfo === '/login_check') {
                    return array (  '_controller' => 'Eglobal\\UserBundle\\Controller\\LoginController::login_checkAction',  '_route' => 'login_check',);
                }

            }

            // logout
            if ($pathinfo === '/logout') {
                return array (  '_controller' => 'Eglobal\\UserBundle\\Controller\\LoginController::logoutAction',  '_route' => 'logout',);
            }

        }

        // register_page
        if ($pathinfo === '/registration') {
            return array (  '_controller' => 'Eglobal\\UserBundle\\Controller\\RegisterController::registerAction',  '_route' => 'register_page',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
